package lt.vcs.classes;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author mariusmerkevicius
 * @since 2017-07-05
 */
public class MyUserInputReaderTest {

    @Test
    public void inputWithOneLetter() throws Exception {
        MyUserInputReader reader = new MyUserInputReader();

        char c = reader.userInput("a");

        assertEquals("a".charAt(0), c);
    }

    @Test
    @Ignore // Dont try it
    public void inputWithTwoLetters() throws Exception {
        MyUserInputReader reader = new MyUserInputReader();

        char c = reader.userInput("ab");

        assertEquals("a".charAt(0), c);
    }
}