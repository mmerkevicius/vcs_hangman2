package lt.vcs.classes;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class StartingWordTest {

    @Test
    public void workingWord() throws Exception {
        StartingWord startingWord = new StartingWord("Marius");

        boolean isChar = startingWord.checkOneChar("M".charAt(0));

        assertTrue(isChar);
    }

    @Test
    public void workingLetter_lowercase() throws Exception {
        StartingWord startingWord = new StartingWord("Marius");

        boolean isChar = startingWord.checkOneChar("m".charAt(0));

        assertTrue(isChar);
    }

    @Test
    public void noSuchLetter() throws Exception {
        StartingWord startingWord = new StartingWord("Marius");

        boolean isChar = startingWord.checkOneChar("f".charAt(0));

        assertFalse(isChar);
    }

    @Test
    public void validLetter_digitSymbol() throws Exception {
        StartingWord startingWord = new StartingWord("M4rius");

        boolean isChar = startingWord.checkOneChar("4".charAt(0));

        assertTrue(isChar);
    }

}