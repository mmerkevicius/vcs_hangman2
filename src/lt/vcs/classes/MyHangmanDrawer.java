package lt.vcs.classes;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class MyHangmanDrawer {

    static public void draw(int lives) {  //
        String[] img = new String[7];
        img[0] = "   0   ";
        img[1] = "   |   ";
        img[2] = "  /|   ";
        img[3] = "  /|\\  ";
        img[4] = "  /    ";
        img[5] = "  /\\  ";

        switch(lives){
            case 5:
//                System.out.println(img[0]);
                //while()
                System.out.println(
                        "  o  " +
                        "     " +
                        "     " +
                        "     " +
                        "     " +
                        "     "
                );
                break;
            case 4:
                System.out.println(img[0]);
                System.out.println(img[1]);
                break;
            case 3:
                System.out.println(img[0]);
                System.out.println(img[2]);
                break;
            case 2:
                System.out.println(img[0]);
                System.out.println(img[3]);
                break;
            case 1:
                System.out.println(img[0]);
                System.out.println(img[3]);
                System.out.println(img[4]);
                break;
            case 0:
                System.out.println(img[0]);
                System.out.println(img[3]);
                System.out.println(img[5]);
                break;
        }
    }


}
