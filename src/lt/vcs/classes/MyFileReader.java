package lt.vcs.classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class MyFileReader {
    private String fileName;

    public MyFileReader(String fileName) {
        this.fileName = fileName;
    }

    public String readFileAsString() throws IOException {
        List<String> names = readFile();
        String fullText = "";
        for (int i = 0; i < names.size(); i++) {
            fullText = fullText + names.get(i);
        }
        return fullText;
    }

    public List<String> readFile() throws IOException {
        List<String> names = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line.length() > 0) {
                    names.add(line);
                }
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
        return names;
    }

    public void printNames(List<String> names) {
        System.out.println(names);
    }

}
