package lt.vcs.classes;

import java.util.Scanner;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class MyUserInputReader {

    Scanner keyboardScanner = new Scanner(System.in);

    public char userInput(String input) {
        System.out.println("Iveskite raide kuria norite patikrinti");
        String userInput = input;

        while (userInput.length() > 1){
            System.out.println("repeat input");
            userInput = input;
        }
        char letter = userInput.charAt(0);
        return letter;
    }

    public char userInput() {
        return userInput(supplyRealUserInput());
    }

    public String supplyRealUserInput() {
        return keyboardScanner.nextLine();
    }

}
