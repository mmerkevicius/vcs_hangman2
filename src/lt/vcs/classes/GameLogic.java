package lt.vcs.classes;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class GameLogic {

    String word;
    int lives = 5;
    int lettersToSpell;

    public void run(String word) {
        StartingWord startingWord = new StartingWord(word);
        MyUserInputReader userInputReader = new MyUserInputReader();
        lettersToSpell = word.length();

        System.out.println("pameginkite atspeti zodi");
        System.out.println(startingWord.getMaskedFullWord());
        System.out.println();
        while (lives > 0 && lettersToSpell > 0) {
            char oneLetter = userInputReader.userInput();
            if (startingWord.checkOneChar(oneLetter)) {
                System.out.println(startingWord.getMaskedFullWord());
                lettersToSpell -= startingWord.letterInWord;
            } else {
                System.out.println(startingWord.getMaskedFullWord());
                lives--;
            }
            System.out.println();
            System.out.println("raidziu liko atspeti: " + lettersToSpell);
            System.out.println("liko gyvybiu: " + lives);
            MyHangmanDrawer.draw(lives);
        }
        if (lives == 0) {
            System.out.println("jus pralosete");
        } else {
            System.out.println("jus atspejote zodi");
        }
    }
}
