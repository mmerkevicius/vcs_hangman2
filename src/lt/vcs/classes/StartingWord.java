package lt.vcs.classes;

/**
 * @author mariusmerkevicius
 * @since 2017-07-04
 */
public class StartingWord {
    private String startingWord;
    private char[] state;
    int letterInWord = 0;

    public StartingWord(String word) {
        this.startingWord = word;
        this.state = new char[startingWord.length()];
        for (int i = 0; i < state.length; i++) {
            state[i] = "_".charAt(0);
        }
    }

    public boolean checkOneChar(char oneLetter) {
        boolean weHaveThatletter = false;
        letterInWord = 0;
        for (int i = 0; i < state.length; i++) {
            if (startingWord.toLowerCase().charAt(i) == oneLetter) {
                state[i] = startingWord.charAt(i);
                weHaveThatletter = true;
                letterInWord++;
            }
            if (startingWord.toUpperCase().charAt(i) == oneLetter) {
                state[i] = startingWord.charAt(i);
                weHaveThatletter = true;
            }
        }
        return weHaveThatletter;
    }

    public String getMaskedFullWord() {
        String fullWord = "";
        for (int i = 0; i < startingWord.length(); i++) {
            fullWord = fullWord + state[i] + " ";
        }
        return fullWord;
    }

}
