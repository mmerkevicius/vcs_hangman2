package lt.vcs;

import lt.vcs.classes.GameLogic;
import lt.vcs.classes.MyFileReader;
import lt.vcs.classes.StartingWord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    /*
    1. Main game logic
    2. Create class to draw hangman
    3. Read file / Output as generic list
    4. Take user input
    5. Game logic
     */

    static Scanner keyboardScanner= new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        startGame();
    }
    public static void startGame() throws IOException {
        MyFileReader fr = new MyFileReader("zodziai.txt");
        List<String> listOfWords = new ArrayList<>();
        listOfWords = fr.readFile();

        Random rand = new Random();
        int randomNumber = rand.nextInt(listOfWords.size());
        String oneWord = listOfWords.get(randomNumber);
        GameLogic gameLogic = new GameLogic();
        gameLogic.run(oneWord);

        System.out.println("Norite pakartori? Iveskite t, jei norite pakartoti");
        String repeatOrNot = keyboardScanner.nextLine();
        if ("t".equals(repeatOrNot)){
            startGame();
        }
    }
}
